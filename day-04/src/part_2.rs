use crate::common::Card;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let cards = input
        .lines()
        .map(|line| Card::from(line))
        .collect::<Vec<Card>>();
    let mut cards_amount = vec![1; cards.len()];
    for card in cards.iter() {
        let found_numbers: usize = card
            .card_numbers
            .iter()
            .filter(|number| card.winning_numbers.contains(number))
            .collect::<Vec<_>>()
            .len();
        let increment = cards_amount.get(card.id - 1).expect("index failed").clone();
        for i in 0..found_numbers {
            if i + card.id  < cards.len()  {
                cards_amount[i + card.id] += increment;
            }
        }
    }
    let sum: usize = cards_amount.iter().sum();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "30";

        assert_eq!(result, sample_output)
    }
}
