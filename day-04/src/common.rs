use regex::Regex;

pub struct Card {
    pub id: usize,
    pub winning_numbers: Vec<usize>,
    pub card_numbers: Vec<usize>,
}

impl From<&str> for Card {
    fn from(value: &str) -> Self {
        let id_regex = Regex::new(r"\d+").expect("failed to compile id regex");
        let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
        // extract id
        let id = id_regex
            .find(value)
            .expect("failed to extract id")
            .as_str()
            .parse::<usize>()
            .expect("failed to parse");
        let value = value
            .split(":")
            .last()
            .expect("failed to extract numbers part");
        let number_split_index = value.find("|").expect("failed to find split");
        let winning_numbers: Vec<usize> = numbers_regex
            .find_iter(value)
            .filter(|number_match| number_match.end() < number_split_index)
            .map(|number_match| {
                number_match
                    .as_str()
                    .parse::<usize>()
                    .expect("failed to parse")
            })
            .collect();
        let card_numbers: Vec<usize> = numbers_regex
            .find_iter(value)
            .filter(|number_match| number_match.end() > number_split_index)
            .map(|number_match| {
                number_match
                    .as_str()
                    .parse::<usize>()
                    .expect("failed to parse")
            })
            .collect();
        Card {
            id,
            winning_numbers,
            card_numbers,
        }
    }
}
