use crate::common::Card;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let cards: Vec<Card> = input.lines().map(|line| Card::from(line)).collect();
    let sum: usize = cards
        .iter()
        .map(|card| {
            let found_numbers: Vec<&usize> = card
                .card_numbers
                .iter()
                .filter(|number| card.winning_numbers.contains(number))
                .collect();
            let mut res: usize = usize::from((found_numbers.len() > 0));
            res = res * 2_usize.pow(found_numbers.len().checked_sub(1).unwrap_or(0) as u32);

            println!("{:?} => {:?}", found_numbers, res);
            res
        })
        .sum();
    Ok(format!("{}", sum))
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "13";

        assert_eq!(result, sample_output)
    }
}
