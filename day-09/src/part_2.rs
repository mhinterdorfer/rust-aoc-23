use crate::common::{calculate_expansion, parse};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_input = parse(input);
    let sum: i32 = parsed_input
        .iter()
        .map(|line| calculate_expansion(line))
        .map(|expanded_row| extrapolate(&expanded_row))
        .sum();
    return Ok(format!("{}", sum));
}

fn extrapolate(expanded_rows: &Vec<Vec<i32>>) -> i32 {
    let mut new_val = 0;
    expanded_rows.iter().rev().skip(1).for_each(|row| {
        new_val = row.first().unwrap()-new_val;
    });
    new_val
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "2";

        assert_eq!(result, sample_output)
    }
}
