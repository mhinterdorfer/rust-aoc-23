pub fn calculate_expansion(line: &Vec<i32>) -> Vec<Vec<i32>> {
    let mut expanded_rows: Vec<Vec<i32>> = vec![line.clone()];
    while !expanded_rows
        .last()
        .unwrap()
        .iter()
        .all(|number| *number == 0)
    {
        let new_row = expanded_rows
            .last()
            .unwrap()
            .windows(2)
            .map(|window| window[1] - window[0])
            .collect::<Vec<i32>>();
        expanded_rows.push(new_row);
    }
    expanded_rows
}
pub fn parse(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|line| {
            line.split(" ")
                .map(|number| number.parse::<i32>().unwrap())
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>()
}
