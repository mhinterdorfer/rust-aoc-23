use std::fmt::format;
use crate::common::{calculate_expansion, parse};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_input = parse(input);
    let sum: i32 = parsed_input
        .iter()
        .map(|line| calculate_expansion(line))
        .map(|expanded_row| extrapolate(&expanded_row))
        .sum();
    return Ok(format!("{}", sum));
}

fn extrapolate(expanded_row: &Vec<Vec<i32>>) -> i32 {
    let mut new_val = 0;
    expanded_row.iter().rev().skip(1).for_each(|row| {
        new_val = new_val + row.last().unwrap();
    });
    new_val
}


#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "114";

        assert_eq!(result, sample_output)
    }
}
