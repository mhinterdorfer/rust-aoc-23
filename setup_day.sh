#! /bin/bash

echo "enter day to create: "
read day_number

echo "enter session cookie value: "
read session_value

padded_day_number=$(printf '%02d' $day_number)

cargo new "day-$padded_day_number"
rm day-$padded_day_number/src/main.rs
cp day-ex/src/* day-$padded_day_number/src/
echo "anyhow = \"1.0.75\"" >>day-$padded_day_number/Cargo.toml
mkdir day-$padded_day_number/assets

curl -b "session=$session_value" -X GET "https://adventofcode.com/2023/day/$day_number/input" >>day-$padded_day_number/assets/input.txt
touch day-$padded_day_number/assets/test_input_1.txt
touch day-$padded_day_number/assets/test_input_2.txt

com.brave.Browser "https://adventofcode.com/2023/day/$day_number"
