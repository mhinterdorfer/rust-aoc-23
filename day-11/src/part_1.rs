use crate::common::{calculate_distance_sum, expand_universe, parse, Entity, PositionedEntity};
use itertools::Itertools;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_input = parse(input);
    let parsed_input = expand_universe(&parsed_input, 2);
    let sum = calculate_distance_sum(parsed_input);
    return Ok(format!("{}", sum));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "374";

        assert_eq!(result, sample_output)
    }
}
