use itertools::Itertools;

pub fn calculate_distance_sum(map: Vec<Vec<PositionedEntity>>) -> i64 {
    let galaxies = map
        .iter()
        .flat_map(|row| {
            row.iter()
                .filter_map(|entity| match entity.entity_type {
                    Entity::Galaxy => Some((entity.position_x, entity.position_y)),
                    Entity::Space => None,
                })
                .collect::<Vec<(i64, i64)>>()
        })
        .collect::<Vec<(i64, i64)>>();
    galaxies
        .into_iter()
        .combinations(2)
        .map(|combination| {
            ((combination.first().unwrap().0 - combination.last().unwrap().0).abs()
                + (combination.first().unwrap().1 - combination.last().unwrap().1).abs())
        })
        .sum::<i64>()
}
pub fn parse(input: &str) -> Vec<Vec<PositionedEntity>> {
    let parsed_input = input
        .lines()
        .enumerate()
        .map(|(line_index, line)| {
            line.chars()
                .enumerate()
                .map(|(char_index, char)| match char {
                    '.' => PositionedEntity {
                        entity_type: Entity::Space,
                        position_x: char_index as i64,
                        position_y: line_index as i64,
                    },
                    '#' => PositionedEntity {
                        entity_type: Entity::Galaxy,
                        position_x: char_index as i64,
                        position_y: line_index as i64,
                    },
                    _ => unreachable!(),
                })
                .collect::<Vec<PositionedEntity>>()
        })
        .collect::<Vec<Vec<PositionedEntity>>>();
    parsed_input
}
pub fn expand_universe(
    input: &Vec<Vec<PositionedEntity>>,
    expansion_rate: i64,
) -> Vec<Vec<PositionedEntity>> {
    let input = expand_empty_rows(input, expansion_rate);
    let input = expand_empty_columns(&input, expansion_rate);
    input
}

fn expand_empty_rows(
    input: &Vec<Vec<PositionedEntity>>,
    expansion_rate: i64,
) -> Vec<Vec<PositionedEntity>> {
    let mut expansion_count = 0;
    input
        .into_iter()
        .map(|row| {
            if row.iter().all(|entity| match entity.entity_type {
                Entity::Galaxy => false,
                Entity::Space => true,
            }) {
                expansion_count += 1;
            }
            row.iter()
                .map(|entity| PositionedEntity {
                    entity_type: entity.entity_type,
                    position_x: entity.position_x,
                    position_y: entity.position_y + expansion_count * (expansion_rate - 1),
                })
                .collect::<Vec<PositionedEntity>>()
        })
        .collect()
}
fn expand_empty_columns(
    input: &Vec<Vec<PositionedEntity>>,
    expansion_rate: i64,
) -> Vec<Vec<PositionedEntity>> {
    let columns_to_expand = (0..input.get(0).unwrap().len())
        .filter(|column| {
            input
                .iter()
                .all(|row| matches!(row.get(*column).unwrap().entity_type, Entity::Space))
        })
        .collect::<Vec<usize>>();
    input
        .iter()
        .map(|row| {
            let mut expansion_count = 0;
            row.iter()
                .enumerate()
                .map(|(index, column)| {
                    if columns_to_expand.contains(&index) {
                        expansion_count += 1;
                    }
                    PositionedEntity {
                        entity_type: column.entity_type,
                        position_x: column.position_x + expansion_count * (expansion_rate - 1),
                        position_y: column.position_y,
                    }
                })
                .collect::<Vec<PositionedEntity>>()
        })
        .collect::<Vec<Vec<PositionedEntity>>>()
}
#[derive(Debug, Copy, Clone)]
pub enum Entity {
    Galaxy,
    Space,
}

#[derive(Debug)]
pub struct PositionedEntity {
    pub entity_type: Entity,
    position_x: i64,
    position_y: i64,
}
