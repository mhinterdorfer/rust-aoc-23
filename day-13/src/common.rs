pub fn find_vertical_mirror_line(map: &Vec<Vec<PositionType>>, tolerance: usize) -> Option<usize> {
    let mut index = 1;
    while index < map.get(0).unwrap().len() {
        let mut tolerance: i32 = tolerance as i32;
        let mut lower_index: i32 = (index - 1) as i32;
        let mut upper_index = index;
        let mut check = true;
        while lower_index >= 0 && upper_index < map.get(0).unwrap().len() {
            let left_column = map
                .iter()
                .map(|row| row.get(lower_index as usize).unwrap())
                .collect::<Vec<&PositionType>>();
            let right_column = map
                .iter()
                .map(|row| row.get(upper_index).unwrap())
                .collect::<Vec<&PositionType>>();
            tolerance -= left_column
                .into_iter()
                .zip(right_column)
                .filter(|(a, b)| **a != **b)
                .count() as i32;
            check = check && tolerance >= 0;
            if !check {
                break;
            }
            lower_index -= 1;
            upper_index += 1;
        }
        if check && tolerance == 0{
            return Some(index);
        }
        index += 1;
    }
    None
}

pub fn find_horizontal_mirror_line(
    map: &Vec<Vec<PositionType>>,
    tolerance: usize,
) -> Option<usize> {
    let mut index = 1;
    while index < map.len() {
        let mut tolerance: i32 = tolerance as i32;
        let mut lower_index: i32 = (index - 1) as i32;
        let mut upper_index = index;
        let mut check = true;
        while lower_index >= 0 && upper_index < map.len() {
            tolerance -= map
                .get(lower_index as usize)
                .unwrap()
                .iter()
                .zip(map.get(upper_index).unwrap())
                .filter(|(a, b)| **a != **b)
                .count() as i32;
            check = check && tolerance >= 0;
            if !check {
                break;
            }
            lower_index -= 1;
            upper_index += 1;
        }
        if check && tolerance == 0{
            return Some(index);
        }
        index += 1;
    }
    None
}

pub fn parse(input: &str) -> Vec<Vec<Vec<PositionType>>> {
    input
        .split("\n\n")
        .map(|map| {
            map.lines()
                .map(|line| {
                    line.chars()
                        .map(|char| match char {
                            '.' => PositionType::Rock,
                            '#' => PositionType::Ash,
                            _ => unreachable!(),
                        })
                        .collect::<Vec<PositionType>>()
                })
                .collect::<Vec<Vec<PositionType>>>()
        })
        .collect()
}

#[derive(Debug, PartialEq)]
pub enum PositionType {
    Ash,
    Rock,
}
