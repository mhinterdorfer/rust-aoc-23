use crate::common::{find_horizontal_mirror_line, find_vertical_mirror_line, parse};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let maps = parse(input);
    let sum = maps
        .iter()
        .map(|map| {
            let horizontal_line = find_horizontal_mirror_line(map, 1);
            let vertical_line = find_vertical_mirror_line(map, 1);
            vertical_line.unwrap_or(0) + 100 * horizontal_line.unwrap_or(0)
        })
        .sum::<usize>();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "400";

        assert_eq!(result, sample_output)
    }
}
