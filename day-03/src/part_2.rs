use crate::common::{parse_input, Number};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (numbers, symbols) = parse_input(input);
    let sum: i32 = symbols
        .iter()
        .map(|symbol| {
            numbers
                .iter()
                .filter(|number| {
                    symbol.position <= number.end + 1
                        && symbol.position >= number.start - 1
                        && symbol.line <= number.line + 1
                        && symbol.line >= number.line - 1
                })
                .collect::<Vec<&Number>>()
        })
        .filter(|numbers| numbers.len() == 2)
        .map(|numbers| {
            let numbers = numbers.to_owned();
            numbers.get(0).expect("failed previous checks").number
                * numbers.get(1).expect("failed previous checks").number
        })
        .sum();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "467835";

        assert_eq!(result, sample_output)
    }
}
