use crate::common::{parse_input, Symbol};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (numbers, symbols) = parse_input(input);
    let numbers_sum: i32 = numbers
        .iter()
        .filter(|number| {
            let found = symbols.iter().find(|symbol| {
                symbol.position <= number.end + 1
                    && symbol.position >= number.start - 1
                    && symbol.line <= number.line + 1
                    && symbol.line >= number.line - 1
            });
            found.is_some()
        })
        .map(|number| number.number)
        .sum();
    return Ok(format!("{}", numbers_sum));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "4361";

        assert_eq!(result, sample_output)
    }
}
