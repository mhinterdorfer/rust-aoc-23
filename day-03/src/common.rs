use regex::Regex;

#[derive(Debug)]
pub struct Number {
    pub number: i32,
    pub line: i32,
    pub start: i32,
    pub end: i32,
}
#[derive(Debug)]
pub struct Symbol<'a> {
    pub line: i32,
    pub position: i32,
    pub symbol: &'a str,
}
pub fn parse_input(input: &str) -> (Vec<Number>, Vec<Symbol>) {
    let number_regex = Regex::new(r"\d+").expect("failed to compile number regex");
    let symbol_regex = Regex::new(r"[^.\d]").expect("failed to compile symbol regex");
    let numbers: Vec<Number> = input
        .lines()
        .enumerate()
        .flat_map(|(line_index, line)| {
            number_regex
                .find_iter(line)
                .map(|number_match| Number {
                    number: number_match
                        .as_str()
                        .parse::<i32>()
                        .expect("failed to parse number"),
                    line: line_index as i32,
                    start: number_match.start() as i32,
                    end: number_match.end() as i32 - 1,
                })
                .collect::<Vec<Number>>()
        })
        .collect();
    let symbols: Vec<Symbol> = input
        .lines()
        .enumerate()
        .flat_map(|(line_index, line)| {
            symbol_regex
                .find_iter(line)
                .map(|symbol_match| Symbol {
                    line: line_index as i32,
                    position: symbol_match.start() as i32,
                    symbol: symbol_match.as_str(),
                })
                .collect::<Vec<Symbol>>()
        })
        .collect();
    (numbers, symbols)
}
