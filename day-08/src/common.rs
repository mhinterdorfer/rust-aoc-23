use itertools::Itertools;
use regex::Regex;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone)]
pub enum Direction {
    Left,
    Right,
}

pub fn parse(input: &str) -> (Vec<Direction>, HashMap<&str, (&str, &str)>) {
    let (directions_line, node_lines) = input.split("\n\n").collect_tuple().unwrap();
    let directions = directions_line
        .chars()
        .map(|char| match char {
            'R' => Direction::Right,
            'L' => Direction::Left,
            _ => unreachable!(),
        })
        .collect::<Vec<Direction>>();
    let node_regex = Regex::new(r"[0-9]{0,2}[A-Z]{1,3}").expect("failed to compile regex");
    let nodes: HashMap<&str, (&str, &str)> = node_lines
        .lines()
        .map(|line| {
            let (name, left, right) = node_regex
                .find_iter(line)
                .map(|node| node.as_str())
                .collect_tuple()
                .unwrap();
            (name, (left, right))
        })
        .collect();
    (directions, nodes)
}
