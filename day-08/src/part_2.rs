use crate::common::{parse, Direction};
use itertools::Itertools;
use std::num;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (directions, nodes) = parse(input);
    let starting_nodes = nodes
        .keys()
        .filter(|key| key.ends_with('A'))
        .map(|key| key.clone())
        .collect::<Vec<&str>>();
    let counts = starting_nodes
        .iter()
        .map(|starting_node| {
            let mut current_node = starting_node.clone();
            let mut counter: usize = 0;
            let mut wrapping_iter = directions.iter().copied().cycle();
            while !current_node.ends_with('Z') {
                counter += 1;
                let next_direction = wrapping_iter.next().unwrap();
                match next_direction {
                    Direction::Left => current_node = nodes.get(current_node).unwrap().0,
                    Direction::Right => current_node = nodes.get(current_node).unwrap().1,
                }
            }
            counter
        })
        .clone();
    let lcm = counts.fold(0usize, |multiple, count| {
        if multiple == 0 {
            count
        } else {
            lcm(multiple, count)
        }
    });

    return Ok(format!("{}", lcm));
}
fn lcm(first: usize, second: usize) -> usize {
    first * (second / gcd(first, second))
}

fn gcd(a: usize, b: usize) -> usize {
    if a == b {
        return a;
    }
    let mut a = a;
    let mut b = b;
    if b > a {
        let temp = a;
        a = b;
        b = temp % b;
    }
    while b > 0 {
        let temp = a;
        a = b;
        b = temp % b;
    }
    return a;
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "6";

        assert_eq!(result, sample_output)
    }
}
