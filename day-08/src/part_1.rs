use crate::common::{parse, Direction};
use itertools::Itertools;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (directions, nodes) = parse(input);
    let mut current_node = "AAA";
    let mut wrapping_iter = directions.iter().cycle();
    let mut counter = 0;
    while current_node != "ZZZ" {
        counter += 1;
        let next_direction = wrapping_iter.next().unwrap();
        match next_direction {
            Direction::Left => current_node = nodes.get(current_node).unwrap().0,
            Direction::Right => current_node = nodes.get(current_node).unwrap().1,
        }
    }
    return Ok(format!("{}", counter));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "2";

        assert_eq!(result, sample_output)
    }
}
