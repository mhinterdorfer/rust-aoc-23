pub fn solve(input: &str) -> anyhow::Result<String>{
    return Ok("42".to_string());
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "42";

        assert_eq!(result, sample_output)
    }
}
