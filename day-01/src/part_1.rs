use regex::Regex;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let pattern = Regex::new(r"[0-9]").expect("failed to compile pattern");
    let sum: i32 = input
        .lines()
        .map(|line| {
            let matches = pattern
                .find_iter(line)
                .map(|item| item.as_str().parse::<u8>().expect("failed parsing"))
                .collect::<Vec<u8>>();
            format!("{}{}", matches[0], matches[matches.len() - 1])
                .parse::<i32>()
                .expect("failed calculating sum")
        })
        .sum();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "142";

        assert_eq!(result, sample_output)
    }
}
