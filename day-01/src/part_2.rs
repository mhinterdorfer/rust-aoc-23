use regex::Regex;
use std::str::FromStr;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let pattern: Regex = Regex::new(r"[0-9]").expect("failed to compile PATTERN");
    let sum: i32 = input
        .lines()
        .map(|line| {
            let line = replace_numbers(line);
            let matches = pattern
                .find_iter(&line)
                .map(|item| item.as_str().parse::<u8>().expect("failed parsing"))
                .collect::<Vec<u8>>();
            format!("{}{}", matches[0], matches[matches.len() - 1])
                .parse::<i32>()
                .expect("failed calculating sum")
        })
        .sum();
    return Ok(format!("{}", sum));
}

fn replace_numbers(text: &str) -> String {
    let mut text: String = String::from_str(text).expect("failed to clone string");
    const WORDS_TO_DIGITS: [(&str, &str); 10] = [
        ("zero", "0"),
        ("one", "1"),
        ("two", "2"),
        ("three", "3"),
        ("four", "4"),
        ("five", "5"),
        ("six", "6"),
        ("seven", "7"),
        ("eight", "8"),
        ("nine", "9"),
    ];
    for (word, digit) in WORDS_TO_DIGITS {
        text = text.replace(word, &format!("{}{}{}", word, digit, word));
    }
    text
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "281";

        assert_eq!(result, sample_output)
    }
}
