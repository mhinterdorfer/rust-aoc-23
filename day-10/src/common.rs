pub fn parse(input: &str) -> (usize, usize, Vec<PipeType>) {
    let mut start_pos = 0;
    let mut width = 0;
    let parsed_input = input
        .lines()
        .enumerate()
        .flat_map(|(line_index, line)| {
            line.chars()
                .enumerate()
                .map(|(column_index, char)| match char {
                    '|' => PipeType::Vertical,
                    '-' => PipeType::Horizontal,
                    'L' => PipeType::NorthEast,
                    'J' => PipeType::NorthWest,
                    '7' => PipeType::SouthWest,
                    'F' => PipeType::SouthEast,
                    '.' => PipeType::Ground,
                    'S' => {
                        start_pos = line_index * line.len() + column_index;
                        width = line.len();
                        PipeType::Start
                    }
                    _ => unreachable!(),
                })
                .collect::<Vec<PipeType>>()
        })
        .collect::<Vec<PipeType>>();
    (start_pos, width, parsed_input)
}
pub fn walk_loop(start_pos: usize, width: usize, pipes: &Vec<PipeType>) -> (usize, Vec<bool>) {
    let mut path = vec![false; pipes.len()];
    path[start_pos] = true;
    let (mut position, mut direction) = {
        if start_pos.checked_sub(width).is_some()
            && matches!(
                pipes[start_pos - width],
                PipeType::Vertical | PipeType::SouthWest | PipeType::SouthEast
            )
        {
            // if one can walk up
            (start_pos - width, Direction::Up)
        } else if matches!(
            pipes[start_pos + width],
            PipeType::Vertical | PipeType::NorthEast | PipeType::NorthWest
        ) {
            // if one can walk down
            (start_pos + width, Direction::Down)
        } else {
            // else only left and right are over... and because one is in a loop, one MUST be able to walk either left or right
            (start_pos - 1, Direction::Left)
        }
    };

    // technically find start again, count steps
    let distance = 1+ unsafe {
        let mut steps = 0;
        loop {
            path[position] = true;
            match (pipes.get(position).unwrap(), direction) {
                (PipeType::Vertical, Direction::Down) => position += width,
                (PipeType::Vertical, Direction::Up) => position -= width,
                (PipeType::Horizontal, Direction::Left) => position -= 1,
                (PipeType::Horizontal, Direction::Right) => position += 1,
                (PipeType::NorthEast, Direction::Down) | (PipeType::SouthEast, Direction::Up) => {
                    position += 1;
                    direction = Direction::Right;
                }
                (PipeType::NorthEast, Direction::Left)
                | (PipeType::NorthWest, Direction::Right) => {
                    position -= width;
                    direction = Direction::Up;
                }
                (PipeType::SouthWest, Direction::Up) | (PipeType::NorthWest, Direction::Down) => {
                    position -= 1;
                    direction = Direction::Left;
                }
                (PipeType::SouthWest, Direction::Right)
                | (PipeType::SouthEast, Direction::Left) => {
                    position += width;
                    direction = Direction::Down;
                }
                (PipeType::Start, _) => break,
                (_, _) => unreachable!(),
            }
            steps += 1;
        }
        steps
    } / 2;
    (distance, path)
}
#[derive(Debug, Eq, PartialEq)]
pub enum PipeType {
    Start,
    Vertical,
    Horizontal,
    NorthEast,
    NorthWest,
    SouthWest,
    SouthEast,
    Ground,
}
#[derive(Debug, Clone, Copy)]
pub enum Direction {
    Up,
    Left,
    Right,
    Down,
}
