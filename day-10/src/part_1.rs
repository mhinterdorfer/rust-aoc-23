use crate::common::{parse, walk_loop, Direction, PipeType};
use std::collections::VecDeque;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (start, width, parsed_input) = parse(input);
    let (distance, _) = walk_loop(start, width, &parsed_input);
    return Ok(format!("{}", distance));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "4";

        assert_eq!(result, sample_output)
    }
}
