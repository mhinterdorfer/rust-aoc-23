use crate::common::{parse, walk_loop, PipeType};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let (start, width, parsed_input) = parse(input);
    let (distance, covered_path) = walk_loop(start, width, &parsed_input);
    let mut inside = false;
    let contained_tiles = parsed_input
        .iter()
        .enumerate()
        .filter(|(pos, cell)| {
            let pipe = unsafe { *covered_path.get_unchecked(*pos) };
            inside &= pos % (width) != 0;
            inside ^= pipe
                && matches!(
                    *cell,
                    PipeType::Vertical | PipeType::SouthEast | PipeType::SouthWest
                );
            inside && (!pipe || **cell == PipeType::Ground) && (pos % (width) != width)
        })
        .count();
    return Ok(format!("{}", contained_tiles));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "10";

        assert_eq!(result, sample_output)
    }
}
