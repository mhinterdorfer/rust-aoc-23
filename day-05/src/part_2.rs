#![recursion_limit = "1000"]
use itertools::{sorted_unstable, Itertools};
use regex::Regex;
use std::collections::{BTreeSet, HashMap, HashSet};
use std::ops::{Add, Range};
struct Converter {
    seeds: Vec<u64>,
    conversion_maps: Vec<ConversionMap>,
}
struct ConversionMap {
    name: String,
    conversions: Vec<Conversion>,
}
struct Conversion {
    destination_range_start: u64,
    source_range_start: u64,
    length: u64,
}
#[derive(PartialEq, Debug)]
struct RangeMapping {
    converted_range: Option<Range<u64>>,
    unconverted_ranges: Vec<Range<u64>>,
}
impl From<&str> for Conversion {
    fn from(value: &str) -> Self {
        let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
        let (destination_range_start, source_range_start, length) = numbers_regex
            .find_iter(value)
            .map(|number| number.as_str().parse::<u64>().unwrap_or(0))
            .collect_tuple()
            .expect("failed iterator");
        Conversion {
            destination_range_start,
            source_range_start,
            length,
        }
    }
}
impl Conversion {
    pub fn map_range_to_ranges(self: &Self, range: &Range<u64>) -> RangeMapping {
        // scenario 4: range is entirely outside the source range
        if (range.end < self.source_range_start)
            || (range.start > self.source_range_start + self.length)
        {
            return RangeMapping {
                converted_range: None,
                unconverted_ranges: vec![range.clone()],
            };
        } else if (range.start >= self.source_range_start)
            && (range.end <= self.source_range_start + self.length)
        {
            // scenario 1: range is entirely within the source range
            let converted_range = range.start - self.source_range_start
                + self.destination_range_start
                ..range.end - self.source_range_start + self.destination_range_start;
            RangeMapping {
                converted_range: Some(converted_range),
                unconverted_ranges: Vec::new(),
            }
        } else if range.start < self.source_range_start {
            if range.end <= self.source_range_start + self.length {
                // scenario 2.1: range starts before the source range, but ends within it
                let converted_range = self.destination_range_start
                    ..range.end - self.source_range_start + self.destination_range_start;
                let unconverted_range = range.start..self.source_range_start - 1;

                RangeMapping {
                    converted_range: Some(converted_range),
                    unconverted_ranges: vec![unconverted_range],
                }
            } else {
                // scenario 3: range starts before the source range, and ends after source + len
                let converted_range =
                    self.destination_range_start..self.destination_range_start + self.length;
                let left_unconverted_range = range.start..self.source_range_start - 1;
                let right_unconverted_range = self.source_range_start + self.length..range.end;

                RangeMapping {
                    converted_range: Some(converted_range),
                    unconverted_ranges: vec![left_unconverted_range, right_unconverted_range],
                }
            }
        } else {
            // scenario 2.2: range starts after the source range, and ends after it
            let converted_range = range.start - self.source_range_start
                + self.destination_range_start
                ..self.destination_range_start + self.length;
            let unconverted_range = self.source_range_start + self.length + 1..range.end;
            RangeMapping {
                converted_range: Some(converted_range),
                unconverted_ranges: vec![unconverted_range],
            }
        }
    }
}
impl ConversionMap {
    pub fn find_ranges(self: &Self, src_range: &Range<u64>) -> Vec<Range<u64>> {
        // go through each conversion and try to map the range to it
        // if there is a match, return the converted range + recurse on the unconverted ranges
        for conversion in &self.conversions {
            let mapping = conversion.map_range_to_ranges(src_range);
            if let Some(converted_range) = mapping.converted_range {
                // Recursively process unconverted ranges
                let mut result_ranges = vec![converted_range];
                let recursive_results: Vec<Range<_>> = mapping
                    .unconverted_ranges
                    .iter()
                    .flat_map(|r| self.find_ranges(r))
                    .collect();
                result_ranges.extend(recursive_results);
                return result_ranges;
            }
        }

        // If no conversion matches, return the original range
        vec![src_range.clone()]
    }
}
pub fn solve(input: &str) -> anyhow::Result<String> {
    let converter = parse(input);
    let mut ranges = converter
        .seeds
        .chunks(2)
        .map(|chunk| chunk[0]..chunk[1] + chunk[0])
        .collect::<Vec<Range<u64>>>();
    for conversion_map in converter.conversion_maps {
        let mut next_ranges: Vec<Range<u64>> = vec![];
        println!("{}", conversion_map.name);
        for range in ranges.iter() {
            let converted_ranges = conversion_map.find_ranges(range);
            next_ranges.extend(converted_ranges);
        }
        ranges = next_ranges
    }
    return Ok(format!(
        "{}",
        ranges.iter().map(|range| range.start).min().unwrap()
    ));
}

fn parse(input: &str) -> Converter {
    let heading_regex = Regex::new(r"[^\d:\n]+:\s*").expect("failed to compile regex");
    let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
    const MAPS: [&str; 7] = [
        "seed_to_soil_map",
        "soil_to_fertilizer_map",
        "fertilizer_to_water_map",
        "water_to_light_map",
        "light_to_temperature_map",
        "temperature_to_humidity_map",
        "humidity_to_location_map",
    ];
    let cleared_text = heading_regex.replace_all(input, "");
    let parts = cleared_text.split("\n\n").collect::<Vec<&str>>();
    assert_eq!(parts.len(), 8);
    let seeds = numbers_regex
        .find_iter(parts.get(0).expect("invalid format"))
        .map(|number| {
            number
                .as_str()
                .parse::<u64>()
                .expect("failed to parse number")
        })
        .collect();
    let conversions: Vec<ConversionMap> = parts
        .iter()
        .skip(1)
        .enumerate()
        .map(|(index, part)| {
            let conversions = part
                .lines()
                .map(|line| Conversion::from(line))
                .collect::<Vec<Conversion>>();
            ConversionMap {
                name: MAPS[index].to_string(),
                conversions,
            }
        })
        .collect();
    Converter {
        seeds: seeds,
        conversion_maps: conversions,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "46";

        assert_eq!(result, sample_output)
    }
}
