use itertools::Itertools;
use regex::Regex;

pub struct Conversion {
    pub destination_range_start: usize,
    pub source_range_start: usize,
    pub length: usize,
}
pub struct Converter {
    pub seeds: Vec<usize>,
    pub seed_to_soil_map: Vec<Conversion>,
    pub soil_to_fertilizer_map: Vec<Conversion>,
    pub fertilizer_to_water_map: Vec<Conversion>,
    pub water_to_light_map: Vec<Conversion>,
    pub light_to_temperature_map: Vec<Conversion>,
    pub temperature_to_humidity_map: Vec<Conversion>,
    pub humidity_to_location_map: Vec<Conversion>,
}
pub fn parse(input: &str) -> Converter {
    let heading_regex = Regex::new(r"[^\d:\n]+:\s*").expect("failed to compile regex");
    let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
    let cleared_text = heading_regex.replace_all(input, "");
    let parts = cleared_text.split("\n\n").collect::<Vec<&str>>();
    assert_eq!(parts.len(), 8);
    let seeds = numbers_regex
        .find_iter(parts.get(0).expect("invalid format"))
        .map(|number| {
            number
                .as_str()
                .parse::<usize>()
                .expect("failed to parse number")
        })
        .collect();
    let (
        seed_to_soil_map,
        soil_to_fertilizer_map,
        fertilizer_to_water_map,
        water_to_light_map,
        light_to_temperature_map,
        temperature_to_humidity_map,
        humidity_to_location_map,
    ) = parts
        .iter()
        .skip(1)
        .map(|part| {
            part.lines()
                .map(|line| Conversion::from(line))
                .collect::<Vec<Conversion>>()
        })
        .collect_tuple()
        .expect("failed iterator");
    Converter {
        seeds,
        seed_to_soil_map,
        soil_to_fertilizer_map,
        fertilizer_to_water_map,
        water_to_light_map,
        light_to_temperature_map,
        temperature_to_humidity_map,
        humidity_to_location_map,
    }
}
impl From<&str> for Conversion {
    fn from(value: &str) -> Self {
        let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
        let (destination_range_start, source_range_start, length) = numbers_regex
            .find_iter(value)
            .map(|number| number.as_str().parse::<usize>().unwrap_or(0))
            .collect_tuple()
            .expect("failed iterator");
        Conversion {
            destination_range_start,
            source_range_start,
            length,
        }
    }
}
impl Conversion {
    pub fn source_to_dest(self: &Self, value: usize) -> usize {
        if value < self.source_range_start || value > self.source_range_start + self.length {
            return value.clone();
        }
        self.destination_range_start + value - self.source_range_start
    }
    pub fn dest_to_source(self: &Self, value: usize) -> usize {
        if value < self.destination_range_start
            || value > self.destination_range_start + self.length
        {
            return value.clone();
        }
        self.source_range_start + value - self.destination_range_start
    }
    pub fn contains_source(self: &Self, value: usize) -> bool {
        value >= self.source_range_start && value <= self.source_range_start + self.length
    }
    pub fn contains_dest(self: &Self, value: usize) -> bool {
        value >= self.destination_range_start && value <= self.destination_range_start + self.length
    }
}
