use crate::common::{parse, Converter};
use itertools::Itertools;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let converter = parse(input);
    let min = get_min_location(&converter);
    return Ok(format!("{}", min));
}

pub fn get_min_location(converter: &Converter) -> usize {
    let soils = converter
        .seeds
        .iter()
        .map(|seed| {
            converter
                .seed_to_soil_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*seed))
                .unwrap()
                .source_to_dest(*seed)
        })
        .unique()
        .collect::<Vec<usize>>();
    let fertilizer = soils
        .iter()
        .map(|soil| {
            converter
                .soil_to_fertilizer_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*soil))
                .unwrap()
                .source_to_dest(*soil)
        })
        .unique()
        .collect::<Vec<usize>>();
    let water = fertilizer
        .iter()
        .map(|fertilizer| {
            converter
                .fertilizer_to_water_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*fertilizer))
                .unwrap()
                .source_to_dest(*fertilizer)
        })
        .unique()
        .collect::<Vec<usize>>();
    let light = water
        .iter()
        .map(|water| {
            converter
                .water_to_light_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*water))
                .unwrap()
                .source_to_dest(*water)
        })
        .unique()
        .collect::<Vec<usize>>();
    let temperatures = light
        .iter()
        .map(|light| {
            converter
                .light_to_temperature_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*light))
                .unwrap()
                .source_to_dest(*light)
        })
        .unique()
        .collect::<Vec<usize>>();
    let humiditys = temperatures
        .iter()
        .map(|temperature| {
            converter
                .temperature_to_humidity_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*temperature))
                .unwrap()
                .source_to_dest(*temperature)
        })
        .unique()
        .collect::<Vec<usize>>();
    let locations = humiditys
        .iter()
        .map(|humidity| {
            converter
                .humidity_to_location_map
                .iter()
                .find_or_first(|conversion| conversion.contains_source(*humidity))
                .unwrap()
                .source_to_dest(*humidity)
        })
        .unique()
        .collect::<Vec<usize>>();
    *locations.iter().min().unwrap()
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "35";

        assert_eq!(result, sample_output)
    }
}
