use regex::Regex;
use crate::common::parse;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let number_replace_regex =Regex::new(r"(\d+)( +)").expect("failed to compile regex");
    let input = number_replace_regex.replace_all(input, "$1");
    let races = parse(input.as_ref());
    let product = races.iter().map(|race|{
        (1..race.duration).map(|speed| speed*(race.duration-speed)).filter(|distance| *distance>race.curr_max_distance).count()
    }).fold(1usize, |product, value| product*value );
    return Ok(format!("{}", product));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "71503";

        assert_eq!(result, sample_output)
    }
}
