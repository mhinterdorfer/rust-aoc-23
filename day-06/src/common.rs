use itertools::Itertools;
use regex::Regex;

#[derive(Debug)]
pub struct Race {
    pub duration: usize,
    pub curr_max_distance: usize,
}
pub fn parse(input: &str) -> Vec<Race> {
    let numbers_regex = Regex::new(r"\d+").expect("failed to compile number regex");
    let (time_line, distance_line) = input.lines().collect_tuple().expect("invalid format");
    let times = numbers_regex
        .find_iter(time_line)
        .map(|number| number.as_str().parse::<usize>().expect("failed parsing"))
        .collect::<Vec<usize>>();
    let distances = numbers_regex
        .find_iter(distance_line)
        .map(|number| number.as_str().parse::<usize>().expect("failed parsing"))
        .collect::<Vec<usize>>();
    assert_eq!(times.len(), distances.len());
    times.iter().zip(distances).map(|(time, distance)| Race{ duration: *time, curr_max_distance: distance }).collect::<Vec<Race>>()
}
