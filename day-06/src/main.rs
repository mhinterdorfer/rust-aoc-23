mod part_1;
mod part_2;
mod common;

fn main() {
    let input = include_str!("../assets/input.txt");
    let part_1 = part_1::solve(input);
    let part_2 = part_2::solve(input);

    match part_1 {
        Ok(result) => {
            println!("The solution to part 1 is {}", result);
        }
        Err(error) => {
            println!(
                "The calculation of part 1 failed with error {}",
                error.to_string()
            );
        }
    }
    match part_2 {
        Ok(result) => {
            println!("The solution to part 2 is {}", result);
        }
        Err(error) => {
            println!(
                "The calculation of part 2 failed with error {}",
                error.to_string()
            );
        }
    }
}
