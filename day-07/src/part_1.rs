use itertools::Itertools;
use std::cmp::Ordering;
use std::fmt::format;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let hands = parse(input);
    let sum: usize = hands
        .iter()
        .sorted_by(|a, b| a.partial_cmp(b).unwrap())
        .enumerate()
        .map(|(index, hand)| (index + 1) * hand.bid)
        .sum();
    return Ok(format!("{}", sum));
}

#[derive(Debug, Ord, Eq)]
struct Hand {
    bid: usize,
    cards: [usize; 5],
}
impl Hand {
    fn get_type(self: &Self) -> usize {
        let mut counts: [usize; 13] = [0; 13];
        self.cards.iter().for_each(|card| counts[*card] += 1);
        if self.cards.iter().all_equal() {
            return 7;
        }
        if counts.iter().find(|count| **count == 4).is_some()
            && counts.iter().find(|count| **count == 1).is_some()
        {
            return 6;
        }
        if counts.iter().find(|count| **count == 3).is_some()
            && counts.iter().find(|count| **count == 2).is_some()
        {
            return 5;
        }

        if counts.iter().find(|count| **count == 3).is_some()
            && counts.iter().filter(|count| **count == 1).count() == 2
        {
            return 4;
        }

        if counts.iter().filter(|count| **count == 2).count() == 2 {
            return 3;
        }
        if counts.iter().filter(|count| **count == 2).count() == 1 {
            return 2;
        }
        1
    }
}
impl PartialEq<Self> for Hand {
    fn eq(&self, other: &Self) -> bool {
        todo!()
    }
}

impl PartialOrd<Self> for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let first_level = self.get_type().cmp(&other.get_type());
        Option::from(if !first_level.is_eq() {
            first_level
        } else {
            self.cards.cmp(&other.cards)
        })
    }
}

impl From<&str> for Hand {
    fn from(value: &str) -> Self {
        let (cards, bid) = value.split(" ").into_iter().collect_tuple().unwrap();
        let cards: [usize; 5] = cards
            .chars()
            .into_iter()
            .map(|char| match char {
                'A' => 12,
                'K' => 11,
                'Q' => 10,
                'J' => 9,
                'T' => 8,
                '9' => 7,
                '8' => 6,
                '7' => 5,
                '6' => 4,
                '5' => 3,
                '4' => 2,
                '3' => 1,
                '2' => 0,
                _ => {
                    panic!("invalid card")
                }
            })
            .collect::<Vec<usize>>()
            .try_into()
            .unwrap();
        let bid = bid.parse::<usize>().unwrap();
        Hand { bid, cards }
    }
}
fn parse(input: &str) -> Vec<Hand> {
    input
        .lines()
        .map(|line| Hand::from(line))
        .collect::<Vec<Hand>>()
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "6440";

        assert_eq!(result, sample_output)
    }
}
