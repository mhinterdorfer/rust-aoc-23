use itertools::Itertools;
use std::cmp::Ordering;
use std::fmt::format;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let hands = parse(input);
    let sum: usize = hands
        .iter()
        .sorted_by(|a, b| a.partial_cmp(b).unwrap())
        .enumerate()
        .map(|(index, hand)| (index + 1) * hand.bid)
        .sum();
    return Ok(format!("{}", sum));
}

#[derive(Debug, Ord, Eq)]
struct Hand {
    bid: usize,
    cards: [usize; 5],
}
impl Hand {

    fn try_best_type(self: &Self) -> usize {
        const REPLACEMENTS: [usize; 12] = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
        *REPLACEMENTS.map(|replacement| {
            let cards = self.cards.iter().map(|card| match card {
                0 => replacement,
                _ => *card
            }).collect::<Vec<usize>>().try_into().unwrap();
            get_hand_type(&cards)
        }).iter().max().unwrap()
    }
}
pub fn get_hand_type(cards: &[usize; 5]) -> usize {
    let mut counts: [usize; 13] = [0; 13];
    cards.iter().for_each(|card| counts[*card] += 1);
    if cards.iter().all_equal() {
        return 7;
    }
    if counts.iter().find(|count| **count == 4).is_some()
        && counts.iter().find(|count| **count == 1).is_some()
    {
        return 6;
    }
    if counts.iter().find(|count| **count == 3).is_some()
        && counts.iter().find(|count| **count == 2).is_some()
    {
        return 5;
    }

    if counts.iter().find(|count| **count == 3).is_some()
        && counts.iter().filter(|count| **count == 1).count() == 2
    {
        return 4;
    }

    if counts.iter().filter(|count| **count == 2).count() == 2 {
        return 3;
    }
    if counts.iter().filter(|count| **count == 2).count() == 1 {
        return 2;
    }
    1
}

impl PartialEq<Self> for Hand {
    fn eq(&self, other: &Self) -> bool {
        todo!()
    }
}

impl PartialOrd<Self> for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let first_level = self.try_best_type().cmp(&other.try_best_type());
        Option::from(if !first_level.is_eq() {
            first_level
        } else {
            self.cards.cmp(&other.cards)
        })
    }
}

impl From<&str> for Hand {
    fn from(value: &str) -> Self {
        let (cards, bid) = value.split(" ").into_iter().collect_tuple().unwrap();
        let cards: [usize; 5] = cards
            .chars()
            .into_iter()
            .map(|char| match char {
                'A' => 12,
                'K' => 11,
                'Q' => 10,
                'T' => 9,
                '9' => 8,
                '8' => 7,
                '7' => 6,
                '6' => 5,
                '5' => 4,
                '4' => 3,
                '3' => 2,
                '2' => 1,
                'J' => 0,
                _ => {
                    panic!("invalid card")
                }
            })
            .collect::<Vec<usize>>()
            .try_into()
            .unwrap();
        let bid = bid.parse::<usize>().unwrap();
        Hand { bid, cards }
    }
}
fn parse(input: &str) -> Vec<Hand> {
    input
        .lines()
        .map(|line| Hand::from(line))
        .collect::<Vec<Hand>>()
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "5905";

        assert_eq!(result, sample_output)
    }
}
