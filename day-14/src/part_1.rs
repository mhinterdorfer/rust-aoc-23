use crate::common::{calculate_north_sum, parse, tilt};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let map = parse(input);
    let map = tilt(&map);
    let sum = calculate_north_sum(map);
    Ok(format!("{}", sum))
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "136";

        assert_eq!(result, sample_output)
    }
}
