use crate::common::{calculate_north_sum, parse, tilt, PositionType};
use std::collections::HashMap;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let mut map = parse(input);
    let mut cache = HashMap::new();
    for i in 1..1_000_000_000 {
        for _j in 0..4{
            map = tilt(&map);
            map = rotate(&map);
        }
        if let Some(seen_at) = cache.insert(map.clone(), i) {
            if (1_000_000_000 - i) % (i - seen_at) == 0 {
                break;
            }
        }
    }
    let sum = calculate_north_sum(map);
    Ok(format!("{}", sum))
}

fn rotate(map: &Vec<Vec<PositionType>>) -> Vec<Vec<PositionType>> {
    let mut newmap = vec![vec![PositionType::Space; map.len()]; map[0].len()];
    for r in 0..map.len() {
        for c in 0..map[0].len() {
            newmap[c][map.len() - 1 - r] = map[r][c].clone();
        }
    }
    newmap
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "64";

        assert_eq!(result, sample_output)
    }
}
