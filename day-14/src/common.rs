pub fn calculate_north_sum(map: Vec<Vec<PositionType>>) -> usize {
    let sum = map
        .iter()
        .rev()
        .enumerate()
        .flat_map(|(south_distance, row)| {
            row.iter()
                .map(|position| match position {
                    PositionType::RoundedRock => south_distance + 1,
                    PositionType::Space => 0,
                    PositionType::Rock => 0,
                })
                .collect::<Vec<usize>>()
        })
        .sum::<usize>();
    sum
}

pub fn tilt(map: &[Vec<PositionType>]) -> Vec<Vec<PositionType>> {
    let mut tilted_map: Vec<Vec<PositionType>> = map.to_vec();
    for (row_index, row) in map.iter().enumerate() {
        for (col_index, col) in row.iter().enumerate() {
            match col {
                PositionType::RoundedRock => {
                    tilted_map[row_index][col_index] = PositionType::Space;
                    let next_position =
                        get_next_position(&tilted_map, row_index, col_index);
                    tilted_map[next_position.0][next_position.1] = PositionType::RoundedRock;
                }
                PositionType::Space | PositionType::Rock => {}
            }
        }
    }
    tilted_map
}

fn get_next_position(
    tilted_map: &[Vec<PositionType>],
    row_index: usize,
    col_index: usize,
) -> (usize, usize) {
            let row = tilted_map[0..row_index]
                .iter()
                .rev()
                .position(|bounding_row| {
                    matches!(
                        bounding_row.get(col_index).unwrap(),
                        PositionType::Rock | PositionType::RoundedRock
                    )
                });
            let row = if let Some(row) = row {
                row_index - row
            } else {
                0
            };
            (row, col_index)
}

pub fn parse(input: &str) -> Vec<Vec<PositionType>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| match char {
                    '.' => PositionType::Space,
                    '#' => PositionType::Rock,
                    'O' => PositionType::RoundedRock,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect()
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum PositionType {
    RoundedRock,
    Space,
    Rock,
}