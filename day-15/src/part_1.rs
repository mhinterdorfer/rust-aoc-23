use crate::common::{hash, parse};

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_input = parse(input);
    let sum: u32 = parsed_input
        .iter()
        .map(|step| step.chars().fold(0u32, |result, char| hash(&char, &result)))
        .sum();
    return Ok(format!("{}", sum));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "1320";

        assert_eq!(result, sample_output)
    }
}
