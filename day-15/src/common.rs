pub fn parse(input: &str) -> Vec<&str> {
    input.split(',').collect()
}
pub fn hash(char: &char, current_value: &u32) -> u32 {
    let ascii_char = *char as u32;
    let mut result = *current_value + ascii_char;
    result *= 17;
    result % 256
}
