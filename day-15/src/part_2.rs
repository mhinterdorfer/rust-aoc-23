use crate::common::{hash, parse};
use regex::Regex;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let steps = parse(input);
    let mut boxes: Vec<Vec<(&str, usize)>> = vec![vec![]; 256];
    let label_regex = Regex::new(r"[a-zA-Z]+").expect("failed to compile label regex");
    let operation_regex = Regex::new(r"[-=]").expect("failed to compile operation regex");
    let focal_length_regex = Regex::new(r"\d+").expect("failed to compile focal length regex");
    steps.iter().for_each(|step| {
        let label = label_regex.find(step).unwrap().as_str();
        let box_number = label.chars().fold(0usize, |result, char| {
            hash(&char, &(result as u32)) as usize
        });
        let operation = operation_regex.find(step).unwrap().as_str();
        match operation {
            "-" => boxes[box_number].retain(|entry| entry.0 != label),
            "=" => {
                let focal_length = focal_length_regex
                    .find(step)
                    .unwrap()
                    .as_str()
                    .parse::<usize>()
                    .unwrap();
                let position_option = boxes[box_number].iter().position(|entry| entry.0 == label);
                if let Some(index) = position_option {
                    boxes[box_number].remove(index);
                    boxes[box_number].insert(index, (label, focal_length));
                } else {
                    boxes[box_number].push((label, focal_length));
                }
            }
            _ => unreachable!(),
        }
    });
    let sum = boxes
        .iter()
        .enumerate()
        .map(|(index, light_box)| {
            let index = index + 1;
            light_box
                .iter()
                .enumerate()
                .map(|(lens_index, lens)| index * (lens_index + 1) * lens.1)
                .sum::<usize>()
        })
        .sum::<usize>();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "145";

        assert_eq!(result, sample_output)
    }
}
