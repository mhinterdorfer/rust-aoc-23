use crate::common::{calculate_arrangements, parse, Position, recursive};
use rayon::prelude::*;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_result = parse(input);
    let sum: usize = parsed_result
        .par_iter()
        .map(|result| expand(&result, 5))
        .map(|result| recursive(&result.0, &result.1))
        .sum();
    return Ok(format!("{}", sum));
}

fn expand(
    expansion_input: &(Vec<Option<Position>>, Vec<usize>),
    expansion_value: usize,
) -> (Vec<Option<Position>>, Vec<usize>) {
    let mut states = vec![];
    for i in 0..expansion_value - 1 {
        states.append(&mut expansion_input.0.clone());
        states.append(&mut vec![None]);
    }
    states.append(&mut expansion_input.0.clone());
    (states, expansion_input.1.repeat(expansion_value))
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "525152";

        assert_eq!(result, sample_output)
    }
}
