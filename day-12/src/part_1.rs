use crate::common::{calculate_arrangements, parse, recursive};
use itertools::Itertools;
use rayon::prelude::*;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let parsed_result = parse(input);
    let sum: usize = parsed_result
        .par_iter()
        .map(|result| recursive(&result.0, &result.1))
        .sum();
    return Ok(format!("{}", sum));
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "21";

        assert_eq!(result, sample_output)
    }
}
