use std::collections::HashMap;
use itertools::Itertools;
use regex::Regex;


pub fn calculate_arrangements(states: &Vec<Option<Position>>, parity: &Vec<usize>) -> usize {
    states
        .iter()
        .map(|state| match state {
            None => {
                vec![Position::Operational, Position::Damaged]
            }
            Some(position) => {
                vec![position.clone()]
            }
        })
        .multi_cartesian_product()
        .filter(|product| check_arrangement(product, parity))
        .count()
}

// Recursive with dynamic programming (intermediate results are cached)
pub fn recursive(states: &Vec<Option<Position>>, parity: &Vec<usize>) -> usize {
    let mut cache: HashMap<(usize, usize), usize> = HashMap::new();
    recurse(states, parity, 0, 0, &mut cache)
}

fn recurse(states: &Vec<Option<Position>>, parity: &Vec<usize>, state_index: usize, parity_index: usize, cache: &mut HashMap<(usize, usize), usize>) -> usize {
    // Exhausted ranges
    if parity_index == parity.len() {
        // All ranges accounted for, no '#' may follow in the end
        if state_index >= states.len() ||
            states[state_index..].iter().all(|s| *s != Some(Position::Damaged))
        {
            return 1;
        } else {
            // Invalid result, there are still '#' that follow
            return 0;
        }
    }

    // Exhausted springs, this path was invalid
    if state_index >= states.len() {
        return 0;
    }

    if let Some(res) = cache.get(&(state_index, parity_index)) {
        return *res;
    }

    // We are at '.', move ahead by one
    let spring = states[state_index];
    if let Some(Position::Operational) = spring {
        return recurse(states, parity, state_index + 1, parity_index, cache);
    }

    // We are now at '#' or '?'
    let range = parity[parity_index];
    // Number of arrangements when assuming we are at a '#'
    let mut arrangements = if can_advance(states, parity, state_index, range) {
        recurse(states, parity, state_index + range + 1, parity_index + 1, cache)
    } else { 0 };

    // We are at a '?', consider skipping
    if spring.is_none() {
        arrangements += recurse(states, parity, state_index + 1, parity_index, cache);
    }

    cache.insert((state_index, parity_index), arrangements);
    arrangements
}
fn can_advance(states: &Vec<Option<Position>>, parity: &Vec<usize>, state_index: usize, parity_index: usize) -> bool {
    let end = state_index + parity_index;
    if end > states.len() {
        return false;
    }
    for i in state_index..end {
        if let Some(Position::Operational) = states[i] {
            return false;
        }
    }
    // Next field is '#', the continuous range is too long
    if let Some(Some(Position::Damaged)) = states.get(end) {
        return false;
    }
    true
}

pub fn check_arrangement(states: &Vec<Position>, parity: &Vec<usize>) -> bool {
    let states_string = states
        .iter()
        .map(|state| match state {
            Position::Operational => '.',
            Position::Damaged => '#',
        })
        .collect::<String>();
    let state_regex = Regex::new(r"#+").unwrap();
    let groups_sizes = state_regex
        .find_iter(states_string.as_str())
        .map(|group| group.as_str().len())
        .collect::<Vec<usize>>();
    groups_sizes.len() == parity.len() && groups_sizes.iter().zip(parity).all(|(a, b)| a == b)
}
pub fn parse(input: &str) -> Vec<(Vec<Option<Position>>, Vec<usize>)> {
    let number_regex = Regex::new(r"\d+").unwrap();
    input
        .lines()
        .map(|line| {
            let (state_format, number_format) = line.split(' ').collect_tuple().unwrap();
            let states = state_format
                .chars()
                .map(|char| match char {
                    '#' => Some(Position::Damaged),
                    '.' => Some(Position::Operational),
                    _ => None,
                })
                .collect::<Vec<Option<Position>>>();
            let numbers = number_regex
                .find_iter(number_format)
                .map(|number| number.as_str().parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            (states, numbers)
        })
        .collect()
}

#[derive(Debug, Copy, Clone, Eq,  PartialEq)]
pub enum Position {
    Operational,
    Damaged,
}
