use crate::common::Game;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let games = input
        .lines()
        .map(|line| Game::from(line))
        .collect::<Vec<Game>>();
    let sum: u32 = games
        .iter()
        .map(|game| {
            game.draws
                .iter()
                .map(|draw| draw.red.unwrap_or(1))
                .max()
                .unwrap_or(1)
                * game
                    .draws
                    .iter()
                    .map(|draw| draw.green.unwrap_or(1))
                    .max()
                    .unwrap_or(1)
                * game
                    .draws
                    .iter()
                    .map(|draw| draw.blue.unwrap_or(1))
                    .max()
                    .unwrap_or(1)
        })
        .sum();
    return Ok(format!("{}", sum));
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_2.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "2286";

        assert_eq!(result, sample_output)
    }
}
