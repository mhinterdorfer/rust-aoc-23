use crate::common::Game;
use itertools::Itertools;

pub fn solve(input: &str) -> anyhow::Result<String> {
    let games = input
        .lines()
        .map(|line| Game::from(line))
        .collect::<Vec<Game>>();
    let (total_red, total_green, total_blue) = (12, 13, 14);
    let sum_impossible: u32 = games
        .iter()
        .filter(|game| {
            game.draws
                .iter()
                .filter(|draw| {
                    draw.red.unwrap_or(0) <= total_red
                        && draw.green.unwrap_or(0) <= total_green
                        && draw.blue.unwrap_or(0) <= total_blue
                })
                .count()
                == game.draws.len()
        })
        .map(|game| game.id)
        .sum();
    Ok(format!("{}", sum_impossible))
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sample() {
        let sample_input = include_str!("../assets/test_input_1.txt");
        let result = solve(sample_input).unwrap();

        // insert expected example output here
        let sample_output = "8";

        assert_eq!(result, sample_output)
    }
}
