use itertools::Itertools;

pub struct Game {
    pub id: u32,
    pub draws: Vec<Draw>,
}

pub struct Draw {
    pub red: Option<u32>,
    pub green: Option<u32>,
    pub blue: Option<u32>,
}

impl From<&str> for Draw {
    fn from(value: &str) -> Self {
        //2 red, 2 green
        let parts: Vec<(&str, &str)> = value
            .split(",")
            .map(|raw_part| {
                raw_part
                    .trim()
                    .split(" ")
                    .into_iter()
                    .collect_tuple::<(&str, &str)>()
                    .expect("wrong parameters")
            })
            .collect();
        assert!(parts.len() < 4);
        let red = parts.iter().find(|part| part.1 == "red").map_or_else(
            || None,
            |part| Some(part.0.parse::<u32>().expect("failed parsing amount")),
        );
        let blue = parts.iter().find(|part| part.1 == "blue").map_or_else(
            || None,
            |part| Some(part.0.parse::<u32>().expect("failed parsing amount")),
        );
        let green = parts.iter().find(|part| part.1 == "green").map_or_else(
            || None,
            |part| Some(part.0.parse::<u32>().expect("failed parsing amount")),
        );

        Draw { red, green, blue }
    }
}

impl From<&str> for Game {
    fn from(value: &str) -> Self {
        // Game 1: 2 red, 2 green; 6 red, 3 green; 2 red, 1 green, 2 blue; 1 red
        let game_draw_split = value.split(":").collect::<Vec<&str>>();
        assert_eq!(game_draw_split.len(), 2);
        // extract game id
        let game_id = game_draw_split[0]
            .split(" ")
            .last()
            .unwrap()
            .parse::<u32>()
            .expect("failed to extract game id");

        // extract draws
        let draws: Vec<Draw> = game_draw_split[1]
            .split(";")
            .map(|draw| draw.trim())
            .map(|draw| draw.into())
            .collect();

        Game { id: game_id, draws }
    }
}
